# SpringForShift
A simple SpringBoot application to build with openShift in a remote repository

# To build from external git repo

**Generate ssh key**

```
$ ssh-keygen -f ~/.ssh/bitbucket-springShift
```

When asked to supply a passphrase, leave it empty as OpenShift cannot use an SSH key that requires a passphrase.

The ssh-keygen command will create two files. The private key file will use the name
supplied and the public key file will have the same basename but with a .pub exten‐
sion added to the end.

Add the .pub key to the bitbucket repository

**Create a secret in OpenShift to hold the private key:**

```
$ oc secrets new-sshauth bitbucket-springshift-ssh --ssh-privatekey=$HOME/.ssh/bitbucket-springShift
```

**Grant the builder service account access to the secret, so it can be used when pulling down the source code to build the application:**

```
$ oc secrets link builder bitbucket-springshift-ssh
```

**Finally, add an annotation to the secret to identify the source code repository it is for:**

```
$ oc annotate secret/bitbucket-springshift-ssh 'build.openshift.io/source-secret-match-uri-1=ssh://git@bitbucket.org:spasqua/springforshift.git'
```

**To create the application, use the SSH URI instead of the HTTPS URI:**

```
$ oc new-app --name springshift --image-stream openshift/wildfly:12.0 --code git@bitbucket.org:spasqua/springforshift.git
```

```
$ oc expose svc/springshift'
```

Because the annotation was added to the secret, the build will automatical


$ oc new-build --name springShift \
--image-stream python:3.5 \
--code https://github.com/openshift-katacoda/blog-django-py \
--source-image blog-wheelhouse \
--source-image-path /opt/app-root/wheelhouse/.:.s2i/wheelhouse

 oc secrets link builder bitbucket-springshift-ssh


## 

# Brief MAVEN recap

To get the dependency tree:

```
mvn dependency:tree
```

To get all the goals from a plugin

```
mvn help:describe -Dplugin=compiler   

mvn help:describe -Dplugin=spring-boot
```

To get all the goals from spring-boot plugin

```
mvn spring-boot:help  
```

Properties in pom.xml
Maven provides properties AKA placeholders that can be used inside pom.xml file. Maven properties are referenced in pom.xml file using the ${property_name} notation. There are two types of properties – implicit and user-defined properties.
Implicit Properties
Implicit properties are properties that are available by default to any Maven project. For example, Maven exposes its Project Object Model properties using the “project.” prefix. To access the artifactId value inside the pom.xml file, you can use the ${project. artifactId} as shown in the following:
<build>
       <finalName>${project.artifactId}</finalName>
</build>
Simillarly, to access properties from settings.xml file, you can use the “settings.” prefix. Finally, the “env.” prefix can be used to access environment variable values. For example, ${env.PATH} will return the value of PATH environment variable.
User-Defined Properties


# Docker build

Docker file

```
FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
```

Build the container

```
docker build -t spasqua/springforshift .
```

Run the container

```
docker run -p 8080:8080 -t spasqua/springforshift 
```

By maven:


```
<plugin>
	<groupId>com.google.cloud.tools</groupId>
	<artifactId>jib-maven-plugin</artifactId>
	  <configuration>       
        <image>spasqua/springforshift</image>
      </to>
    </configuration>
</plugin>
```		
			
To build docker using maven:

```
mvn jib:build
```			
			

