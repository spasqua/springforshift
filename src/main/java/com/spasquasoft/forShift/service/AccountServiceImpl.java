package com.spasquasoft.forShift.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spasquasoft.forShift.service.dao.Account;
import com.spasquasoft.forShift.service.dao.AccountDao;

@Service
public class AccountServiceImpl implements AccountService {
	
	@Autowired
	private AccountDao dao;
	
    @Override
	public ArrayList<Account>  getAll() {
    return dao.getAll();	
    }
    
    @Override
	public boolean insert(Account account) {
    return dao.insertAccount(account);	
    }
    
    @Override
	public boolean delete(Account account) {
    return dao.deleteAccount(account);	
    }

	@Override
	public Account get(Account account) {
	return dao.getAccount(account);
	}
	
	@Override
	public boolean modify(Account account) {
	return dao.modify(account);
	}	
    
    
    

}
