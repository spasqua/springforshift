package com.spasquasoft.forShift.service.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


import javax.sql.DataSource;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/**
 * Dao 
 * @author spasqua
 *
 */
@Repository
public class AccountDaoImpl implements AccountDao {
	
	private final DataSource dataSource;
	
	public AccountDaoImpl(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public ArrayList<Account> getAll() {
		
        ArrayList<Account> accounts=new ArrayList<Account>();

        String query = "SELECT * FROM ACCOUNT";

        try (Connection con = dataSource.getConnection();
             Statement st = con.createStatement();
             ResultSet rs = st.executeQuery(query)) {

            while (rs.next()) {
                
                System.out.printf("%s %s %s", rs.getString(1),
                        rs.getString(2), rs.getString(3));
                Account account=new Account();
                account.setId(rs.getString(1));
                account.setName(rs.getString(2));
                account.setAddress(rs.getString(3));
                accounts.add(account);
                
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        
        return accounts;
	}

	
	@Override
	public boolean insertAccount(Account account) {
		
		String query = "INSERT INTO ACCOUNT (ID,NAME,ADDRESS) VALUES ( '"
					+account.getId()+"','"
					+account.getName()+"','"
					+account.getAddress()+"')";		

        boolean ret=false;

        try  {
        	Connection con = dataSource.getConnection();
        	Statement st = con.createStatement();
        	 ret = st.execute(query);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        
        return ret; 

	}

	@Override
	public boolean deleteAccount(Account account) {

		String query = "DELETE FROM ACCOUNT WHERE ID='"+account.getId()+"'";
       
        boolean ret=false;
        
        try  {
        	Connection con = dataSource.getConnection();
        	Statement st = con.createStatement();
                ret = st.execute(query);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        
        return ret;
	}
	
	@Override
	public Account getAccount(Account account) {
		
		String query = "SELECT * FROM ACCOUNT WHERE ID='"+account.getId()+"'";
        
        try (Connection con = dataSource.getConnection();
             Statement st = con.createStatement();
             ResultSet rs = st.executeQuery(query)) {

            while (rs.next()) {
                
                System.out.printf("%s %s %s", rs.getString(1),
                        rs.getString(2), rs.getString(3));
                
                account.setId(rs.getString(1));
                account.setName(rs.getString(2));
                account.setAddress(rs.getString(3));
                
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        
        return account;    
		
	}	
	
	
	@Override
	public boolean modify(Account account) {	
		
        String query = "UPDATE ACCOUNT SET NAME = '"+account.getName()+"', ADDRESS = '"+account.getAddress()+"' WHERE ID='"+account.getId()+"'";
        
        boolean ret=false;
        
        try  {
             Connection con = dataSource.getConnection();
             Statement st = con.createStatement();
                ret = st.execute(query);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        
        return ret;		
		
	}
	

}
