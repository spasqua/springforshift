package com.spasquasoft.forShift.service.dao;

import java.util.ArrayList;


public interface AccountDao {

	ArrayList<Account> getAll();

	boolean insertAccount(Account account);

	boolean deleteAccount(Account account);
	
	Account getAccount(Account account);
	
	boolean modify(Account account);
	
}
