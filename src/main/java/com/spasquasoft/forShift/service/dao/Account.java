package com.spasquasoft.forShift.service.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Account {

	private String name;
	
	private String address;
	
	private String id;


	public Account(String id) {
		super();
		this.id = id;
	}	
	

}
