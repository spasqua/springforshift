package com.spasquasoft.forShift.service;

import java.util.ArrayList;

import com.spasquasoft.forShift.service.dao.Account;

public interface AccountService {

	ArrayList<Account> getAll();

	boolean insert(Account account);

	boolean delete(Account account);
	
	Account get(Account account);
	
	boolean modify(Account account);

}