package com.spasquasoft.forShift;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringForShiftApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringForShiftApplication.class, args);
	}

}
