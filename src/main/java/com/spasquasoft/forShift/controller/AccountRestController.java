package com.spasquasoft.forShift.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spasquasoft.forShift.service.AccountService;
import com.spasquasoft.forShift.service.dao.Account;


@RestController
public class AccountRestController {

	@Autowired
	AccountService accountService;
	
    @GetMapping("/jsonlist")
    public ArrayList<Account> listAccount() {
        return accountService.getAll();
    }    
}
