package com.spasquasoft.forShift;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

import javax.servlet.Filter;

@Configuration
public class SpringForShiftConfiguration {

	@Bean
	public Filter logFilter() {
		CommonsRequestLoggingFilter filter = new
				CommonsRequestLoggingFilter();
		
		filter.setIncludeQueryString(true);
		filter.setIncludePayload(true);
		filter.setMaxPayloadLength(5120);
		return filter;
	}
}
